/*******************************************************************************
********************************************************************************
***                                                                          ***
***     Name    -       IR Remote Controler NEC Decoder                      ***
***     Author  -       Tymofii Chashurin                                    ***
***     File    -       NEC_IR.c                                             ***
***                                                                          ***
********************************************************************************
*******************************************************************************/

#include "NEC_IR.h"


// NEC Decoder variables
static uint8_t      bitn = 0;               // Bit number/repeat code number
static uint8_t      decoding_state = 0;     // What is decoding now? (adress, command, rep code)
static uint8_t      system = 0;             // System 8-bit adress
static uint8_t      inv_system = 0;         // Inverted 8-bit system
static uint8_t      command = 0;            // 8-bit command
static uint8_t      inv_command = 0;        // Inverted 8-bit command
static uint8_t      dec_ready = 0;          // Decoding is ready flag
static uint8_t      rep_code = 0;           // Quantity of repeat code sequencies


/* General System Init Function
 *
 */
void NEC_Decoder_Init(void)
{
  LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SYSCFG);
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);
  NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
  
  SystemClock_Config();

  GPIO_Init();

  NEC_Timer_Init();
}


/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);

  if(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_2) {
  Error_Handler();  
  }

  LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE1);
  LL_RCC_HSE_Enable();

  // Wait till HSE is ready
  while(LL_RCC_HSE_IsReady() != 1) {};

  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE, LL_RCC_PLLM_DIV_4, 64, LL_RCC_PLLP_DIV_2);
  LL_RCC_PLL_Enable();

  // Wait till PLL is ready
  while(LL_RCC_PLL_IsReady() != 1) {};

  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);

  // Wait till System clock is ready
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL) {};

  LL_Init1msTick(64000000);
  LL_SetSystemCoreClock(64000000);
}


/**
  * GPIO Initialization Function  
  */
static void GPIO_Init(void)
{
  LL_EXTI_InitTypeDef EXTI_InitStruct = {0};
  LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

  // GPIO Ports Clock Enable
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOD);

  LL_GPIO_ResetOutputPin(GPIOD, LL_GPIO_PIN_12|LL_GPIO_PIN_13|LL_GPIO_PIN_14|LL_GPIO_PIN_15 
                          |LL_GPIO_PIN_0|LL_GPIO_PIN_1|LL_GPIO_PIN_2|LL_GPIO_PIN_3 
                          |LL_GPIO_PIN_4|LL_GPIO_PIN_5|LL_GPIO_PIN_6|LL_GPIO_PIN_7);

  GPIO_InitStruct.Pin = LL_GPIO_PIN_12|LL_GPIO_PIN_13|LL_GPIO_PIN_14|LL_GPIO_PIN_15 
                          |LL_GPIO_PIN_0|LL_GPIO_PIN_1|LL_GPIO_PIN_2|LL_GPIO_PIN_3 
                          |LL_GPIO_PIN_4|LL_GPIO_PIN_6|LL_GPIO_PIN_7;
  GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
  GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_LOW;
  GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
  GPIO_InitStruct.Pull = LL_GPIO_PULL_NO;
  LL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_5);
  LL_GPIO_SetPinMode(GPIOB, LL_GPIO_PIN_5, LL_GPIO_MODE_OUTPUT);

  LL_SYSCFG_SetEXTISource(LL_SYSCFG_EXTI_PORTC, LL_SYSCFG_EXTI_LINE1);

  EXTI_InitStruct.Line_0_31 = IR_RECEIVER_LINE;
  EXTI_InitStruct.LineCommand = ENABLE;
  EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
  EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_RISING_FALLING;
  LL_EXTI_Init(&EXTI_InitStruct);

  // Enable a pull-up resistor at the IR receiver's input pin
  LL_GPIO_SetPinPull(IR_RECEIVER_PORT, IR_RECEIVER_PIN, LL_GPIO_PULL_UP);

  // Configure the IR pin as an input
  LL_GPIO_SetPinMode(IR_RECEIVER_PORT, IR_RECEIVER_PIN, LL_GPIO_MODE_INPUT);

  // EXTI1_IRQn interrupt configuration
  NVIC_SetPriority(EXTI1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),7, 0));
  NVIC_EnableIRQ(EXTI1_IRQn);
}


/**
  * Timer 3 Initialization Function
  */
static void NEC_Timer_Init(void)
{
  LL_TIM_InitTypeDef TIM_InitStruct = {0};

  // Reset the timer peripheral unit.
  LL_TIM_DeInit(NEC_Timer);

  // Peripheral clock enable
  LL_APB1_GRP1_EnableClock(RCC_APB_Periph_NEC_Timer);

  TIM_InitStruct.Prescaler = PRESCALER_10_US;
  TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP; // Up counter
  TIM_InitStruct.Autoreload = 0xFFFF; // The maximum possible counter's value
  TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV1;
  LL_TIM_Init(TIM3, &TIM_InitStruct);

  LL_TIM_DisableARRPreload(NEC_Timer);

  LL_TIM_SetClockSource(NEC_Timer, LL_TIM_CLOCKSOURCE_INTERNAL);
  LL_TIM_DisableMasterSlaveMode(NEC_Timer);

  // Set a compare value for channel 1
  LL_TIM_OC_SetCompareCH1(NEC_Timer, TIME_120_MS); // 120 ms timeout

  // Enable Compare interrupt for timer 3
  LL_TIM_EnableIT_CC1(NEC_Timer);

  // TIM3_IRQn interrupt configuration
  NVIC_SetPriority(NEC_Timer_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),7, 0));
  NVIC_EnableIRQ(NEC_Timer_IRQn);
}


// This function returns a value of variable "bitn"
//
uint8_t Get_NEC_bitn_state(uint8_t bitn_state)
{
  return bitn;
}


/* This function returns a value of variable "system"
 *
 */
uint8_t Get_NEC_sys_state(uint8_t sys_state)
{
  return system;
}


/* This function returns a value of variable "command"
 *
 */
uint8_t Get_NEC_com_state(uint8_t com_state)
{
  return command;
}


/* 
 *
 */
void reset_nec_timer(void)
{
  LL_TIM_SetCounter(NEC_Timer, 0);  // Reset timer
}


/* 
 *
 */
uint16_t read_nec_timer(void)
{
  return LL_TIM_GetCounter(NEC_Timer); // Get the pulse len
}


/* 
 *
 */
void start_nec_timer(void)
{
  LL_TIM_EnableCounter(NEC_Timer);
}


/* 
 *
 */
void stop_nec_timer(void)
{
  LL_TIM_DisableCounter(NEC_Timer);
}


/* This functon stores the received "one" bits
 *
 */
void one_is_received(void)
{
  if (bitn < START_OF_COM)
  {
    if (bitn < START_OF_INV_SYS)     // Receive system address bits
    {
      system = ((system >> 1) | 0x80);
    }
    else
    {
      inv_system = ((inv_system >> 1) | 0x80);
    }
  }
  else
  {
    if (bitn < START_OF_INV_COM)     // Receive command bits
    { 
      command = ((command >> 1) | 0x80);
    }
    else
    {
      inv_command = ((inv_command >> 1) | 0x80);
    }
  }
}


/* This functon stores the received "zero" bits
 *
 */
void zero_is_received(void)
{
  if (bitn < START_OF_COM)
  {
    if (bitn < START_OF_INV_SYS)   // Receive system address bits
    {
      system = ((system >> 1) & 0x7F); 
    }
    else 
    {
      inv_system = ((inv_system >> 1) & 0x7F);
    }
  }
  else
  {
    if (bitn < START_OF_INV_COM)   // Receive command bits
    {
      command = ((command >> 1) & 0x7F);
    }
    else
    {  
      inv_command = ((inv_command >> 1) & 0x7F);
    }
  }
}


/* This function returns the NEC 
 * decoder to its initial state
 */
void NEC_Timer_IRQ_Handler (void)
{
  // Clear Timer interrupt flags
  NVIC_ClearPendingIRQ(NEC_Timer_IRQn);
  LL_TIM_ClearFlag_CC1(NEC_Timer);

  stop_nec_timer();
  reset_nec_timer();

  // clear NEC decoder variables
  bitn = 0;
  decoding_state = 0;
  system = 0;
  command = 0;
  inv_system = 0;
  inv_command = 0;
  dec_ready = 0;
  rep_code = 0;
  
  LED_Control();
}


/* This function handles
 * the IR receiver's output
 */
void NEC_External_IRQ_Handler (void)
{
  uint16_t len = 0;

  NVIC_ClearPendingIRQ(NEC_EXTI_IRQn);
  LL_EXTI_ClearFlag_0_31(IR_RECEIVER_LINE);

  len = read_nec_timer();                       // Get the pulse len

  switch (decoding_state)
  {
  case AGC_START:
    start_nec_timer();                          // Start 9 ms pulse measurement
    decoding_state = AGC_END;                   // Begin decoding
    return;
        
  case AGC_END:
    reset_nec_timer();                          // Reset timer
    if ((len > MIN_9_MS) && (len < MAX_9_MS))   // ... 9 ms AGC
    {
      decoding_state = SHORT_PAUSE;             // then begin a 4,5 ms..
      return;                                   // ..pulse measurement
    }
    break;

  case SHORT_PAUSE:
    reset_nec_timer();                          // Reset timer
    if ((len > MIN_4_5_MS) && (len < MAX_4_5_MS))
    {
      decoding_state = NEC_ADDR_COMM;           // Begin system adress decode
      return;      
    }
    break;

  case NEC_ADDR_COMM:
    if ((len > MIN_560_US) && (len < MAX_560_US) &&
       (bitn == MAX_BITS) &&
       ((system & inv_system) == 0) &&
       ((command & inv_command) == 0))
    {
      decoding_state = LONG_PAUSE_1;            // Begin a long pause measurement
      reset_nec_timer();                        // Reset timer
      bitn = DECODE_IS_OK;            
      LED_Control();
      return;                                
    }
    else
    {
      decoding_state = NEC_BITS_DEC;
      return;
    }
    break;
      
  case NEC_BITS_DEC:                            // Bits receiving
    reset_nec_timer();                          // Reset timer
    decoding_state = NEC_ADDR_COMM;                                
    bitn++;

    if ((len > MIN_2_25_MS) && (len < MAX_2_25_MS))
    {
      one_is_received();                        // Received bit is "1"
      return;
    }
    else if ((len > MIN_1_125_MS) && (len < MAX_1_125_MS))
    {
      zero_is_received();                       // Received bit is "0"
      return;
    }
    break;
      
  case LONG_PAUSE_1:
    reset_nec_timer();                          // Reset timer
    if ((len > MIN_40_MS) && (len < MAX_40_MS)) // P
    {                                           // A
      decoding_state = REPEAT_AGC;              // U
      return;                                   // S
    }                                           // E
    break;

  case REPEAT_AGC:
    reset_nec_timer();                          // Reset timer
    if ((len > MIN_9_MS) && (len < MAX_9_MS))
    {                                           // R
      decoding_state = REPEAT_SPACE;            // E
      return;                                   // P
    }                                           // E
    break;                                      // A
                                                // T
  case REPEAT_SPACE:
    reset_nec_timer();                          // Reset timer
    if ((len > MIN_2_25_MS) && (len < MAX_2_25_MS))
    {                                           // C
      decoding_state = REPEAT_BURST;            // O
      return;                                   // D
    }                                           // E
    break;                                      
                                                
  case REPEAT_BURST:
    reset_nec_timer();                          // Reset timer
    if ((len > MIN_560_US) && (len < MAX_560_US))
    {                                           // R
      decoding_state = LONG_PAUSE_2;            // E
      rep_code++;                               // P
      return;                                   // E
    }                                           // A
    break;                                      // T
                                                // .
  case LONG_PAUSE_2:                            // .
    reset_nec_timer();                          // Reset timer
    if ((len > MIN_98_MS) && (len < MAX_98_MS))
    {                                           // C
      decoding_state = REPEAT_AGC;              // O
      return;                                   // D
    }                                           // E
    break;
     
  default:
    reset_nec_timer();                          // Reset timer
    decoding_state = FAULT;                     // Indicate an error
    dec_ready = 0;
    break;
  }
}
