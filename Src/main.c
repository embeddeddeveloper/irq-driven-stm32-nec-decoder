/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/**
  * The application entry point.
  */
int main(void)
{
  NEC_Decoder_Init();

  while (1)
  {
  }
}


/**
 * Driving LEDs function
 */
void LED_Control (void)
{
  uint16_t tmp = 0;
  static volatile uint8_t state[4];         // LED states
  static volatile uint8_t flag[4];          // Button press flags

  if (Get_NEC_bitn_state(tmp) == 0x55)      // DECODE_IS_OK
  {
    if (Get_NEC_sys_state(tmp) == 0)        // SYSTEM
    {
      tmp = Get_NEC_com_state(tmp);
      LL_GPIO_SetOutputPin(GPIOD, tmp);

      tmp &= LL_GPIO_PIN_5;
      LL_GPIO_SetOutputPin(GPIOB, tmp);

      switch (Get_NEC_com_state(tmp))
      {    
      case STBY:
        if (flag[0] == 0)
        {
          state[0] ^= 1;
          flag[0] = 1;
        }
        break;
        
      case MUTE:
        if (flag[1] == 0)
        {
          state[1] ^= 1;
          flag[1] = 1;
        }
        break;
        
      case INPUT:
        if (flag[2] == 0)
        {
          state[2] ^= 1;
          flag[2] = 1;
        }
        break;
      
      case SOUND:
        if (flag[3] == 0)
        {
          state[3] ^= 1;
          flag[3] = 1;
        }
        break;
      }
    }
  }
  else
  {
    flag[0] = 0;
    flag[1] = 0;
    flag[2] = 0;
    flag[3] = 0;
    LL_GPIO_ResetOutputPin(GPIOD, LL_GPIO_PIN_0 | LL_GPIO_PIN_1 | LL_GPIO_PIN_2 |
                                  LL_GPIO_PIN_3 | LL_GPIO_PIN_4 | LL_GPIO_PIN_5 |
                                  LL_GPIO_PIN_6 | LL_GPIO_PIN_7);
    LL_GPIO_ResetOutputPin(GPIOB, LL_GPIO_PIN_5);
  }
  
  if (state[0] == 1)
  {
    Set_Green;
  }
  else
  {
    Reset_Green;
  }
  if (state[1] == 1)
  {
    Set_Orange;
  }
  else
  {
    Reset_Orange;
  }
  if (state[2] == 1)
  {
    Set_Red;
  }
  else
  {
    Reset_Red;
  }
  if (state[3] == 1)
  {
    Set_Blue;
  }
  else
  {
    Reset_Blue;
  }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}
