/*******************************************************************************
********************************************************************************
***                                                                          ***
***     Name    -       IR Remote Controler NEC Decoder                      ***
***     Author  -       Timofey Chashurin                                    ***
***     Date    -       10.07.2014                                           ***
***     File    -       NEC_IR.h                                             ***
***                                                                          ***
********************************************************************************
*******************************************************************************/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __NEC_IR_H
#define __NEC_IR_H

#include "main.h"

/*
 * Exported functions
 */
void NEC_Decoder_Init(void);
//uint8_t Check_Time(uint8_t data);
void one_is_received(void);
void zero_is_received(void);
void reset_nec_timer(void);
uint16_t read_nec_timer(void);
void start_nec_timer(void);
void stop_nec_timer(void);
uint8_t Get_NEC_bitn_state(uint8_t bitn_state);
uint8_t Get_NEC_sys_state(uint8_t sys_state);
uint8_t Get_NEC_com_state(uint8_t com_state);

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void GPIO_Init(void);
static void NEC_Timer_Init(void);

enum decoding_state {	
						AGC_START,
						AGC_END,
						SHORT_PAUSE,
						NEC_ADDR_COMM,
						NEC_BITS_DEC,
						LONG_PAUSE_1,
						REPEAT_AGC,
						REPEAT_SPACE,
						REPEAT_BURST,
						LONG_PAUSE_2,
						FAULT
					};

/*
 * Exported constants
 */ 
// NEC Decoder TypeDefs
#define         IR_RECEIVER_PORT           GPIOC
#define         IR_RECEIVER_PIN            LL_GPIO_PIN_1
#define         IR_RECEIVER_LINE           LL_EXTI_LINE_1 
#define         NEC_Timer                  TIM3
#define         RCC_APB_Periph_NEC_Timer   LL_APB1_GRP1_PERIPH_TIM3 
#define         NEC_Timer_IRQ_Handler      TIM3_IRQHandler 
#define         NEC_Timer_IRQn             TIM3_IRQn
#define         NEC_External_IRQ_Handler   EXTI1_IRQHandler
#define 	NEC_EXTI_IRQn 	           EXTI1_IRQn

// NEC Decoder Constants
#define         SYSTEM                     0
#define         FAULT                      0xFF
#define         MAX_BITS                   32
#define         START_OF_INV_SYS           9
#define         START_OF_COM               17
#define         START_OF_INV_COM           25
#define         DECODE_IS_OK               0x55

#define 		INPUT_TIM_FREQUENCY		   64000000 // 64 MHz Timer Clock
#define 		FREQ_10_US				   100000
#define 		TIME_SLOT_US			   10
#define         PULSE_LENGTH_US			   560				

#define         PRESCALER_10_US            (INPUT_TIM_FREQUENCY / FREQ_10_US)
#define         TIME_560_US                (PULSE_LENGTH_US / TIME_SLOT_US)
#define         DEV_560_US                 TIME_560_US / 4
#define         MIN_560_US                 (TIME_560_US - DEV_560_US)
#define         MAX_560_US                 (TIME_560_US + DEV_560_US)

#define         TIME_1_125_MS              (TIME_560_US * 2)
#define         DEV_1_125_MS               (TIME_1_125_MS / 8)
#define         MIN_1_125_MS               (TIME_1_125_MS - DEV_1_125_MS)
#define         MAX_1_125_MS               (TIME_1_125_MS + DEV_1_125_MS)

#define         TIME_2_25_MS               225
#define         DEV_2_25_MS                (TIME_2_25_MS / 8)
#define         MIN_2_25_MS                (TIME_2_25_MS - DEV_2_25_MS)
#define         MAX_2_25_MS                (TIME_2_25_MS + DEV_2_25_MS)

#define         TIME_4_5_MS                (TIME_2_25_MS * 2)
#define         DEV_4_5_MS                 (TIME_4_5_MS / 8)
#define         MIN_4_5_MS                 (TIME_4_5_MS - DEV_4_5_MS)
#define         MAX_4_5_MS                 (TIME_4_5_MS + DEV_4_5_MS)

#define         TIME_9_MS                  (TIME_4_5_MS * 2)
#define         DEV_9_MS                   (TIME_9_MS / 8)
#define         MIN_9_MS        	       (TIME_9_MS - DEV_9_MS)
#define         MAX_9_MS                   (TIME_9_MS + DEV_9_MS)

#define         TIME_40_MS                 (TIME_560_US * 71)
#define         DEV_40_MS                  (TIME_40_MS / 10)
#define         MIN_40_MS                  (TIME_40_MS - DEV_40_MS)
#define         MAX_40_MS                  (TIME_40_MS + DEV_40_MS)

#define         TIME_98_MS                 (TIME_560_US * 175)
#define         DEV_98_MS                  (TIME_98_MS / 10)
#define         MIN_98_MS				   (TIME_98_MS - DEV_98_MS)
#define         MAX_98_MS				   (TIME_98_MS + DEV_98_MS)

#define         TIME_120_MS                (TIME_560_US * 214)

#endif /* __NEC_IR_H */
